<?php 
include_once('config.php');
?>
<?php
session_start();
 
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: home.php");
    exit;
}
// Include config file
include 'config.php';
 
$username = $password = "";
$username_err = $password_err = "";


if($_SERVER["REQUEST_METHOD"] == "POST"){

    if(empty(trim($_POST["email"]))){
        $username_err = "Please enter username.";
    } else{
        $username = trim( $_POST['email']);
    }
    
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST['password']);
    }
    
    if($_POST["email"] !='' && $_POST["password"] !=''){
        $sql="select * from member where email='".$username."' and password='".$password."'";
       
              if ($result=mysqli_query($conn,$sql))
              {
              $rowcount=mysqli_num_rows($result);
          
              } 
                if($rowcount == 1){                    
                
                            session_start();
                            
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                            // Redirect user to home page
                            header("location: home.php");
                        
                } else{
                    $username_err_acc = "Username or password mismatch.";
                }
                mysqli_free_result($result);
            
            
    }
    
   
}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style.css" >
<title>Educurve Login</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-8 login">
<h1>Login</h1>
<form name="contact-form"  method="post" action="index.php" id="login-form">
<div class="form-group ">
<label for="exampleInputEmail1">Email address</label>
<input type="email" class="form-control" name="email" placeholder="Email" required>
<p style=<?php echo (!empty($username_err)) ? 'display:block;color:red' : 'display:none'; ?>><?php echo $username_err ?></p>
</div>
<div class="form-group">
<label for="password">password</label>
<input type="password" class="form-control" name="password" placeholder="password" required>
<p style=<?php echo (!empty($password_err)) ? 'display:block;color:red' : 'display:none'; ?>><?php echo $password_err ?></p>
</div>

<button type="submit" class="btn btn-primary" name="submit" value="Submit" id="submit_form">Submit</button>
<a href="signup.php">Register</a>
<img src="img/loading.gif" id="loading-img">
</form>
<p style=<?php echo (!empty($username_err_acc)) ? 'display:block;color:red' : 'display:none'; ?>><?php echo $username_err_acc ?></p>
<div class="response_msg"></div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $('.login').css('margin-top',$(window).height()/4);
$("#login-form input").blur(function(){
var checkValue = $(this).val();
if(checkValue != '')
{
$(this).css("border","1px solid #eeeeee");
}
});
});
</script>
</body>
</html>