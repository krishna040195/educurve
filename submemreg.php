<?php 
include_once('config.php');
session_start();
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style.css" >
<title>Educurve submember registration</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-8 register">
<h1>Sign Up</h1>
<form name="contact-form" action="" method="post" id="contact-form">
<div class="form-group">
<label for="Name">Name</label>
<input type="text" class="form-control" name="name" placeholder="Name" required>
</div>
<div class="form-group">
<label for="exampleInputEmail1">Email address</label>
<input type="email" class="form-control" name="email" placeholder="Email" required>
</div>
<div class="form-group">
<label for="password">Grade</label>
<input type="text" class="form-control" name="grade" placeholder="grade" required>
</div>
<input type="hidden" name="flag" value="submember">
<input type="hidden" name="parent" value="<?php echo $_SESSION["username"];?>">
<button type="submit" class="btn btn-primary" name="submit" value="Submit" id="submit_form">Submit</button>
<img src="img/loading.gif" id="loading-img">
</form>
<div class="response_msg"></div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $('.register').css('margin-top',$(window).height()/4);
$("#contact-form").on("submit",function(e){
e.preventDefault();
if($("#contact-form [name='name']").val() === '')
{
$("#contact-form [name='name']").css("border","1px solid red");
}
else if ($("#contact-form [name='email']").val() === '')
{
$("#contact-form [name='email']").css("border","1px solid red");
}
else
{
$("#loading-img").css("display","block");
var sendData = $( this ).serialize();
$.ajax({
type: "POST",
url: "get_response.php",
data: sendData,
success: function(data){
$("#loading-img").css("display","none");
$(".response_msg").text(data);
$(".response_msg").slideDown().fadeOut(3000);
$("#contact-form").find("input[type=text], input[type=email]").val("");
var delay = 3000; 
var url = "index.php"
setTimeout(function(){ window.location = url; }, delay);

}
});
}
});
$("#contact-form input").blur(function(){
var checkValue = $(this).val();
if(checkValue != '')
{
$(this).css("border","1px solid #eeeeee");
}
});
});
</script>
</body>
</html>
<?php } else{

header('Location:index.php');
} ?>